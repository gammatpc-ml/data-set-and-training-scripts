# Data Set and Training Scripts


## Description
Here we provide sample code for the paper _Gamma Ray Source Localization For A Time Projection Chamber Telescope Using Convolutional Neural Networks_ (2022). We include scripts that train the head and direction models for the 200 um pitch, 3cm drift, 500 keV case. The scripts to train the models are identical, except for modifying the parameters `energy`, `drift_distance`, and `pitch` around line 30 of the code. Storage limits prevent us from uploading data - data for 100,000 tracks for a particular energy is approximately 11 GB - though it is available upon request. File directory names have been redacted, and the custom simulated detector response code has been excluded.

## Help
Additional code and data is available upon request to the authors.
