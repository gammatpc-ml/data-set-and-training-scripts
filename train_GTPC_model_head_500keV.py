#!/usr/bin/env python
# coding: utf-8

import numpy as np
import matplotlib.pyplot as plt
import pickle as pl
import pandas as pd
import os
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import random_split, DataLoader, TensorDataset
import time

import sys
sys.path.insert(1, '[File path to detector response code]')

energy = 500
drift_distance = 5
pitch = 3 # Hundred micrometers

root = "[File path to save]".format(pitch, drift_distance)
if not os.path.isdir(root):
    raise Exception("{} is not a valid directory".format(root))

def get_default_device():
    if torch.cuda.is_available():
        return torch.device('cuda')
    else:
        return torch.device('cpu')
    
def to_device(data, device):
    if isinstance(data, (list,tuple)):
        return [to_device(x, device) for x in data]
    return data.to(device, non_blocking=True)

class DeviceDataLoader():
    def __init__(self, dl, device):
        self.dl = dl
        self.device = device
        
    def __iter__(self):
        for b in self.dl: 
            yield to_device(b, self.device)

    def __len__(self):
        return len(self.dl)
    
def accuracy(outputs, labels):
    # Calculate percent error, on average
    acc = 1 - np.mean(np.abs((outputs.cpu().detach().numpy() - labels.cpu().detach().numpy())/labels.cpu().detach().numpy()))
    return acc
    
def plot_losses(history):
    losses = [x['val_loss'] for x in history]
    plt.plot(losses, '-x')
    plt.xlabel('epoch')
    plt.ylabel('loss')
    plt.title('Loss vs. No. of epochs');
    
def plot_losses(history):
    losses = [x['val_loss'] for x in history]
    plt.plot(losses, '-x')
    plt.xlabel('epoch')
    plt.ylabel('loss')
    plt.title('Loss vs. No. of epochs');

device = get_default_device()
print(device)
sys.stdout.flush()

# Data Import and Exploration


datadict = np.load("[File path to data]".format(pitch, drift_distance, energy),
                           allow_pickle=True)
myReadout_train = torch.tensor(datadict['readout_train'], dtype=torch.float32) 
myOrigin_train = torch.tensor(datadict['normheads_train'], dtype=torch.float32)



# Data preprocessing

inputs = myReadout_train
targets = myOrigin_train

dataset = TensorDataset(inputs, targets)


# Create Tensor Data Set (split to train/test/val)

# Create validation set 

val_size = int(len(myOrigin_train) * 0.05)
train_size = len(myOrigin_train) - val_size

# Split the data
train_ds, val_ds = random_split(dataset, [train_size, val_size])
len(train_ds), len(val_ds)


# Data Loader

batch_size = 64

train_loader = DataLoader(train_ds, batch_size, shuffle=True)
val_loader = DataLoader(val_ds, batch_size)

train_loader = DeviceDataLoader(train_loader, device)
val_loader = DeviceDataLoader(val_loader, device)



# Define Test Model

sideLength = 30
input_size = sideLength**3
output_size = 3
hidden_size = 500

class CNN(nn.Module):
    def __init__(self):
        super().__init__()

        self.cnn_stack = nn.Sequential(
            nn.Conv3d(1, 32, kernel_size=3, stride=1), 
            nn.ReLU(),
            nn.Dropout3d(0.25),
            nn.MaxPool3d(2), 
            nn.BatchNorm3d(32),
            
            nn.Conv3d(32, 64, kernel_size=3, stride=1),
            nn.ReLU(),
            nn.Dropout3d(0.25),
            nn.MaxPool3d(2),
            nn.BatchNorm3d(64),

            nn.Conv3d(64, 128, kernel_size=3, stride=1),
            nn.ReLU(),
            nn.Dropout3d(0.25),
            nn.BatchNorm3d(128),

            nn.Flatten(), 
            nn.Dropout(0.25),
            nn.Linear(8192, 4096),
            nn.ReLU(),
            nn.Linear(4096, 1024),
            nn.ReLU(),
            nn.Linear(1024, output_size),
        )

    def forward(self, x):                         
        return self.cnn_stack(x)
    
    def training_step(self, batch):
        inputs, targets = batch 
        # Generate predictions
        out = self(inputs)          
        # Calcuate loss
        loss = F.mse_loss(out, targets) 
        return loss
    
    def validation_step(self, batch):
        with torch.no_grad():
            inputs, targets = batch
            # Generate predictions
            out = self(inputs)
            # Calculate loss
            loss = F.mse_loss(out, targets)
        return {'val_loss': loss.detach()}
        
    def validation_epoch_end(self, outputs):
        batch_losses = [x['val_loss'] for x in outputs]
        epoch_loss = torch.stack(batch_losses).mean()   # Combine losses
        return {'val_loss': epoch_loss.item()}
    
    def epoch_end(self, epoch, result, num_epochs):
        # Print result every 10th epoch
        if (epoch) % 10 == 0 or epoch == num_epochs-1:
            print("Epoch [{}], val_loss: {:.4f}".format(epoch, result['val_loss']))    


# Train Model and Evaluate

def evaluate(model, val_loader):
    model.eval()
    outputs = [model.validation_step(batch) for batch in val_loader]
    return model.validation_epoch_end(outputs)

def fit(epochs, lr, model, train_loader, val_loader, opt_func=torch.optim.Adam, threshold=1e-4, patience=4):
    history = []
    optimizer = opt_func(model.parameters(), lr)
    scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, 'min', factor=0.25, \
                                                           patience=patience, threshold=threshold, threshold_mode='abs', verbose=True)
    min_loss = float("inf")
    lookback = round(patience*2.5)
    for epoch in range(epochs):
        # Training Phase 
        model.train()
        for batch in train_loader:
            loss = model.training_step(batch)
            loss.backward()
            optimizer.step()
            optimizer.zero_grad()
        # Validation phase
        result = evaluate(model, val_loader)
        scheduler.step(result['val_loss'])
        model.epoch_end(epoch, result, epochs)
        history.append(result)
        if epoch % 5 == 0 and (not epoch == 0):
            print("Epoch", epoch, "done", time.strftime("%H:%M:%S", time.localtime()))
            sys.stdout.flush()
        if result['val_loss'] < min_loss:
            min_loss = result['val_loss']
            saveAll(model, history, True)
        elif epoch > lookback and all(threshold >= diff for diff in (np.array([val_loss['val_loss'] for val_loss in history[-lookback:]])- min_loss)):
            # Early stopping if val loss remains stagnant for round(2.5*patience) epochs
            print("Early stopping at Epoch", epoch, time.strftime("%H:%M:%S", time.localtime()), "with val_loss", result['val_loss'])
            sys.stdout.flush()
            break
            
# Train model now

cnn = to_device(CNN(), device)

def saveAll(model, hist, savehist):
    if savehist:
        np.save(root + "GTPC_CNN_model_head_{}keV_history".format(energy), hist)
    torch.save(model.state_dict(), root + "GTPC_CNN_model_head_{}keV".format(energy))

epochs = 100
lr = 1e-3
fit(epochs, lr, cnn, train_loader, val_loader);
